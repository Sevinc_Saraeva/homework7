public class RoboCat extends Pet{
     public RoboCat(String  nickname){
         super(nickname);
         super.setSpecies(Species.ROBOCAT);

     }
    @Override
    public void respond() {
        System.out.println("Hello, owner. I am RoboCat. My name is " + super.getNickname() + ". I miss you! ");
    }
}
