import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human []children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children = new Human[1];
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public boolean addChild(Human child){
        if(isArrayReady(this.children)) {
            this.children[this.children.length-1] = child;
            return true;
        }
        this.children = copyArray(this.children);
        this.children[this.children.length-1] = child;
        return true;
    }

    public boolean isArrayReady(Human [] ar){
        if(ar[ar.length-1]== null){
            return true;
        }
        return false;
    }
    public Human [] copyArray(Human [] ar){
        Human [] ar2 = new Human[ar.length+1];
        ar2 = Arrays.copyOf(ar,ar.length+1);
        return ar2;
    }
    public boolean deleteChild(int index){
        this.children[index] = null;
        return true;
    }
    public int countFamily (){
        return this.children.length+2;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Family))
            return false;
        if (obj == this)
            return true;
        return this.getMother().getName() == ((Family) obj).getMother().getName() && this.getFather().getName() == ((Family) obj).getFather().getName();
    }


    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                '}';
    }
}
