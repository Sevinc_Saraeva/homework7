public final class Man extends Human {


    public Man() {
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public void repairCar(){
        System.out.println(super.getName()+ " is repair car");
    }
    @Override
    public void greetPet() {
        System.out.println("hello, " + super.getFamily().getPet().getNickname());
    }
}
