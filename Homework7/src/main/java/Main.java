public class Main {

    public static void main(String[] args) {
      Cat cat = new Cat("cat1");
      String nickname = cat.getNickname();
      cat.setNickname("new name");
       Fish fish = new Fish("fish");
       fish.setAge(3);
       fish.setSpecies(Species.FISH);
       String nickname_fish = fish.getNickname();

       RoboCat roboCat = new RoboCat("robocat");
       roboCat.setAge(4);
        String nickname1 = roboCat.getNickname();
        roboCat.setNickname("nickname2");
    }
}
