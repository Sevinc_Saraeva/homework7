public class Fish extends Pet{
    public Fish() {
        super();
    }

    public Fish(String nickname ) {
        super(nickname);
        super.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am Fish. My name is + " + super.getNickname() + ". I miss you! ");
        }
    }



