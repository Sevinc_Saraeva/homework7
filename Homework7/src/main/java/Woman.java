public final class Woman extends Human{

    public Woman() {
        super();
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }
    public void makeup(){
        System.out.println(super.getName() + " is doing make up");
    }

    @Override
    public void greetPet() {
        System.out.println("hello, " + super.getFamily().getPet().getNickname());
    }
}
