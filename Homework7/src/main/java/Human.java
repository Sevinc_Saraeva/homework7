
import java.util.Arrays;



public class Human {
    private String name;
    private String surname;
    private  int year;
    private  int iq_level;
    private  Family family;
    private String[][] Shedule;

    public Human() {
    }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year , Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }
    public Human(String name, String surname, int year, int iq_level, String[][] shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq_level = iq_level;

        this.Shedule = shedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq_level() {
        return iq_level;
    }

    public void setIq_level(int iq_level) {
        this.iq_level = iq_level;
    }

    public String[][] getShedule() {
        return Shedule;
    }

    public void setShedule(String[][] shedule) {
        Shedule = shedule;
    }

    public void greetPet(){
        System.out.println("Hello, " + this.family.getPet().getNickname());

    }
    public void describePet(){
        System.out.print("I have " + this.family.getPet()+ ", he is " + this.family.getPet().getAge() + " years old. ");
        if(this.family.getPet().getTrickLevel()<50) System.out.println("He is almost not sly");
        else System.out.println("He is sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq_level=" + iq_level +
                ", schedule= " + Arrays.toString(Shedule)+
                '}';
    }
}

